INSERT INTO menu (title, url, icon, parent_id, role, permission_id)
VALUES ('Dashboard', NULL, NULL, NULL, NULL, NULL);

INSERT INTO roles(NAME, guard_name) VALUES('admin', 'web');
INSERT INTO roles(NAME, guard_name) VALUES('user', 'web');

INSERT INTO permissions(NAME, guard_name) VALUES('add article', 'web');
INSERT INTO permissions(NAME, guard_name) VALUES('edit article', 'web');
INSERT INTO permissions(NAME, guard_name) VALUES('delete article', 'web');
INSERT INTO permissions(NAME, guard_name) VALUES('view article', 'web');

INSERT INTO role_has_permissions(permission_id, role_id) VALUES(1, 1);
INSERT INTO role_has_permissions(permission_id, role_id) VALUES(2, 1);
INSERT INTO role_has_permissions(permission_id, role_id) VALUES(3, 1);
INSERT INTO role_has_permissions(permission_id, role_id) VALUES(4, 1);
INSERT INTO role_has_permissions(permission_id, role_id) VALUES(4, 2);

TRUNCATE users;
INSERT INTO users(NAME, email, PASSWORD) VALUES('user1', 'user1@gmail.com', '$2y$10$Sgrj/OXi.aZvdpxIiGimBu4r6irkBVnacMb/KK7f8dnwzj/zV254.');
INSERT INTO users(NAME, email, PASSWORD) VALUES('user2', 'user2@gmail.com', '$2y$10$Sgrj/OXi.aZvdpxIiGimBu4r6irkBVnacMb/KK7f8dnwzj/zV254.');

TRUNCATE model_has_roles;
INSERT INTO model_has_roles(role_id, model_type, model_id) VALUES(1, 'App\\Models\\User', 1); -- user1 is admin
INSERT INTO model_has_roles(role_id, model_type, model_id) VALUES(2, 'App\\Models\\User', 2); -- user2 is user

TRUNCATE menus;
INSERT INTO menus(title, url, icon, parent_id, role_id, permission_id) VALUES('Dashboard', '/dashboard', 'home', NULL, '[1,2]', NULL);
INSERT INTO menus(title, url, icon, parent_id, role_id, permission_id) VALUES('Pentadbiran', '/pentadbiran', 'setting', NULL, '[1]', NULL);
INSERT INTO menus(title, url, icon, parent_id, role_id, permission_id) VALUES('Pengurusan Menu', '/menu', 'setting', 2, '[1]', NULL);
INSERT INTO menus(title, url, icon, parent_id, role_id, permission_id) VALUES('Bangsa', '/bangsa', 'setting', 3, '[1]', NULL);
INSERT INTO menus(title, url, icon, parent_id, role_id, permission_id) VALUES('Pendaftaran', '/pendaftaran', 'setting', NULL, NULL, '[1]');

