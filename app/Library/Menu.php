<?php
namespace App\Library;
use App\Models\Menu as Mymenu;
use Illuminate\Http\Request;

class Menu {
    public static function gen() {
        $path = "/".request()->path();
        $menus = Mymenu::whereNull('parent_id')->get(); // menu level 1
        $str = '';
        $roles = self::getRoles();
        //$permissions = self::getPemission();
        //dd($permissions);
        foreach ($menus as $menu) {
            if (! empty($menu->role_id)) {
                // check by role
                foreach ($roles as $role) {
                    if (in_array($role, json_decode($menu->role_id))) {
                        // user role match menu role
                        if ($menu->url == $path) {
                            $str .= '<b><a href="' . $menu->url . '">'.$menu->title . '</a></b><br>';
                        } else {
                            $str .= '<a href="' . $menu->url . '">' . $menu->title . '</a><br>';
                        }
                        // check sub-menu (level 2, 3, ...)
                        $str .= self::subMenu($menu->id, 2);
                    }
                }
            }
        }
        return $str;
    }

    // public static function getPemission() {
    //     $user_id = auth()->user()->id;
    //     // all roles of logged-in user
    //     $roles = \DB::table('model_has_roles')->where('model_id', $user_id)
    //     ->pluck('role_id')->toArray();
    //     $permissions = \DB::table('role_has_permissions')->whereIn('role_id', $roles)
    //                     ->pluck('permission_id')->toArray();
    //     return $permissions;
    // }

    // menu level 2, 3, ...
    public static function subMenu($id, $level) {
        $path = "/".request()->path();
        $menus = Mymenu::where('parent_id', $id)->get();
        $str = '';
        $roles = self::getRoles();
        foreach ($menus as $menu) {
            foreach ($roles as $role) {
                if (in_array($role, json_decode($menu->role_id))) {
                    if ($level == 2) {
                        $str .= '--';
                    } else if ($level == 3) {
                        $str .= '---';
                    }

                    if ($menu->url == $path) {
                        $str .= '<b><a href="' . $menu->url . '">' . $menu->title . '</a></b><br>';
                    } else {
                        $str .= '<a href="' . $menu->url . '">' . $menu->title . '</a><br>';
                    }

                    // user role match menu role

                    // check sub-menu (level 2, 3, ...)
                    $str .= self::subMenu($menu->id, $level + 1);
                }
            }
        }
        return $str;
    }

    // return all roles of logged-in user
    public static function getRoles() {
        $user_id = auth()->user()->id;
        $roles = \DB::table('model_has_roles')
                ->where('model_id', $user_id)
                ->pluck('role_id')
                ->toArray();
        return $roles;
    }
}
