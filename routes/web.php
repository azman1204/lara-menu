<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::view('/login', 'login');
Route::post('/login', function(Request $r) {
    // echo \Hash::make('1234');
    // echo $r->email;
    $credentials = [
        'email' => $r->email,
        'password' => $r->password
    ];

    if(\Auth::attempt($credentials)) {
        //echo 'success';
        return redirect('/dashboard');
    } else {
        //echo 'failed';
        return redirect('/login');
    }
});

Route::get('/dashboard', function() {
    return view('dashboard');
});

Route::get('/menu', function() {
    return view('menu');
});

